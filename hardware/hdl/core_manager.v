module core_manager #(
    parameter UART_PRESCALER=868
) (
    input clk,
    input rstn,
    //comet core control
    output comet_rst,
    output comet_stall,
    output comet_en,
    //instruction memory access port
    output [23:0] inst_mem_addr,
    output [31:0] inst_mem_data_in,
    input [31:0] inst_mem_data_out,
    output inst_mem_we,
    //UART IO
    input uart_rx,
    output uart_tx,
    //instruction memory bus MUX control
    output imem_mux_select, //0: core, 1: core manager
    //UART pins MUX control
    output uart_mux_sel    //0: core, 1: core manager
);
//command byte definition
localparam WRITE_CMD=8'hAA;
localparam READ_CMD=8'h55;
localparam START_CMD=8'hFA;

//FSM states definition
localparam RESET=0;
localparam GET_CMD=1;
localparam CMD_DISPATCH=2;
//write command branch of the FSM
localparam GET_WRITE_ADDRESS=3;
localparam GET_WRITE_SIZE=4;
localparam GET_WRITE_DATA=5;
localparam WRITE_END_CHECK=6;
//read command branch of the FSM
localparam GET_READ_ADDRESS=7;
localparam GET_READ_SIZE=8;
localparam READ_WORD_0=9;
localparam READ_WORD_1=10;
localparam READ_WORD_2=11;
localparam SEND_WORD=12;
localparam WAIT_TX_DONE = 13;
localparam READ_END_CHECK=14;
//start command branch of the FSM
localparam START_CPU=15;
localparam NUM_STATES=15;

//wires and regs
reg [$clog2(NUM_STATES)-1:0] state;
reg [23:0] current_addr;
reg [31:0] current_word;
reg [7:0] command;
reg [2:0] byte_counter; //needs to count up to 4 and not 3... see the read command logic 
reg [7:0] word_counter;
reg [7:0] word_limit;

//registered outputs
//core control
reg _comet_rst, _comet_stall, _comet_en;
assign comet_rst = _comet_rst;
assign comet_stall = _comet_stall;
assign comet_en = _comet_en;
//memory bus
reg _inst_mem_we;
assign inst_mem_we = _inst_mem_we;
reg [23:0] _inst_mem_addr;
assign inst_mem_addr = _inst_mem_addr;
reg [31:0] _inst_mem_data_in;
assign inst_mem_data_in = _inst_mem_data_in;
//mux control
reg _imem_mux_select;
assign imem_mux_select = _imem_mux_select;
reg _uart_mux_sel;
assign uart_mux_sel = _uart_mux_sel;

//instantiate the UART rx and tx drivers
wire [7:0] uart_rx_data;
reg [7:0] uart_tx_data;
wire uart_rx_data_valid;
reg uart_tx_data_valid;
wire uart_tx_done;

uart #(.PRESCALER(UART_PRESCALER)) uart (    .clk(clk),
                                                //rx
                                                .uart_rx(uart_rx),
                                                .uart_rx_data(uart_rx_data),
                                                .uart_rx_data_valid(uart_rx_data_valid),
                                                //tx
                                                .uart_tx(uart_tx),
                                                .uart_tx_data(uart_tx_data),
                                                .uart_tx_data_valid(uart_tx_data_valid),
                                                .uart_tx_done(uart_tx_done));

//controller FSM
always @(posedge clk) begin
    //check reset condition
    if(!rstn) begin
        state <= RESET;
    end
    else begin
        case(state)
            RESET:
            begin
                //stop the core
                _comet_rst = 1'b1;  //active high comet rst line
                _comet_stall = 1'b1;    //make sure the core stalls
                _comet_en = 1'b0;   //disable coree FSM, just in case
                //assume control of the instruction memory bus
                _imem_mux_select = 1'b1;
                //make we won't write anything to the inst mem
                _inst_mem_addr <= 1'b0;
                //assume control of the UART lines
                _uart_mux_sel <= 1'b1;
                //wait for a byte to arrive at the uart input
                state <= GET_CMD;
            end
            GET_CMD:
            begin
                //clear the imem interface (no writes)
                _inst_mem_we <= 1'b0;
                if(uart_rx_data_valid) begin
                    //get the command byte
                    command <= uart_rx_data; 
                    state <= CMD_DISPATCH;
                end
            end
            CMD_DISPATCH:
            begin
                //by default go back to the GET_CMD state
                state <= GET_CMD;
                case(command)
                    WRITE_CMD:
                    begin
                        state <= GET_WRITE_ADDRESS;
                        //reset the byte counter
                        byte_counter <= 0;
                    end
                    READ_CMD:
                    begin
                        state <= GET_READ_ADDRESS;
                        //reset the byte counter
                        byte_counter <= 0;
                    end
                    START_CMD:
                    begin
                        state <= START_CPU;
                    end
                endcase
            end
            //write command states
            GET_WRITE_ADDRESS:  //address LSB first
            begin
                if(uart_rx_data_valid) begin
                    //populate the current byte of the current address
                    case(byte_counter) 
                        0:
                        begin
                            current_addr[7:0] <= uart_rx_data;
                            byte_counter <= 1;
                        end
                        1:
                        begin
                            current_addr[15:8] <= uart_rx_data;
                            byte_counter <= 2;
                        end
                        2:
                        begin
                            current_addr[23:16] <= uart_rx_data;
                            //reset the byte counter
                            byte_counter <= 0;
                            //go to the next state
                            state <= GET_WRITE_SIZE;
                        end
                    endcase
                end
            end
            GET_WRITE_SIZE:
            begin
                if(uart_rx_data_valid) begin
                    //store the transfer size
                    word_limit <= uart_rx_data;   //TODO: bug here if we ask for 0 words
                    //reset the word counter
                    word_counter <= 0;
                    //go to the next state
                    state <= GET_WRITE_DATA;
                end
            end
            GET_WRITE_DATA:     //data LSB first
            begin
                //clear the imem interface (no writes)
                _inst_mem_we <= 1'b0;
                if(uart_rx_data_valid) begin
                    case(byte_counter)
                        0:
                        begin
                            current_word[7:0] <= uart_rx_data;
                            byte_counter <= 1;
                        end
                        1:
                        begin
                            current_word[15:8] <= uart_rx_data;
                            byte_counter <= 2;
                        end
                        2:
                        begin
                            current_word[23:16] <= uart_rx_data;
                            byte_counter <= 3;
                        end
                        3:
                        begin
                            current_word[31:24] <= uart_rx_data;
                            //increment the word counter
                            word_counter = word_counter + 1;
                            //go to the next state
                            state <= WRITE_END_CHECK;
                        end
                    endcase
                end
            end
            WRITE_END_CHECK:
            begin
                //write the current word to the memory
                _inst_mem_addr <= current_addr+word_counter-1;
                _inst_mem_data_in <= current_word;
                _inst_mem_we <= 1'b1;
                //check if we have written enough words in the memory
                if(word_counter < word_limit) begin
                    state <= GET_WRITE_DATA;    //not enough data has been written, get some more
                    //reset the byte_counter
                    byte_counter <= 0;
                end
                else begin
                    state <= GET_CMD;
                end
            end
            //READ command states
            GET_READ_ADDRESS:
            begin
                if(uart_rx_data_valid) begin
                    //populate the current byte of the current address
                    case(byte_counter) 
                        0:
                        begin
                            current_addr[7:0] <= uart_rx_data;
                            byte_counter <= 1;
                        end
                        1:
                        begin
                            current_addr[15:8] <= uart_rx_data;
                            byte_counter <= 2;
                        end
                        2:
                        begin
                            current_addr[23:16] <= uart_rx_data;
                            //reset the byte counter
                            byte_counter <= 0;
                            //reset the word counter
                            word_counter <= 0;
                            //go to the next state
                            state <= GET_READ_SIZE;
                        end
                    endcase
                end
            end
            GET_READ_SIZE:
            begin
                if(uart_rx_data_valid) begin
                    //store the transfer size
                    word_limit <= uart_rx_data;   //TODO: bug here if we ask for 0 words
                    //go to the next state
                    state <= READ_WORD_0;
                end
            end
            READ_WORD_0:
            begin
                //get memory data
                _inst_mem_we <= 1'b0;
                _inst_mem_addr <= current_addr + word_counter;
                state <= READ_WORD_1;
            end
            READ_WORD_1:
            begin
                //current_word <= inst_mem_data_out;
                //reset the byte counter
                byte_counter <= 0;
                //send the memory word to the uart
                state <= READ_WORD_2;
            end
            READ_WORD_2:
            begin
                current_word <= inst_mem_data_out;
                state <= SEND_WORD;   
            end
            SEND_WORD:
            begin
                case(byte_counter)
                    0:
                    begin
                        //write data to the uart
                        uart_tx_data <= current_word[7:0];  //memory data LSB first
                        uart_tx_data_valid <= 1'b1;
                        byte_counter <= byte_counter + 1;
                    end
                    1:
                    begin
                        //write the next byte
                        uart_tx_data <= current_word[15:8];
                        uart_tx_data_valid <= 1'b1;
                        byte_counter <= byte_counter + 1;
                    end
                    2:
                    begin
                        //write the next byte
                        uart_tx_data <= current_word[23:16];
                        uart_tx_data_valid <= 1'b1;
                        byte_counter <= byte_counter + 1;
                    end
                    3:
                    begin
                        //write the next byte
                        uart_tx_data <= current_word[31:24];
                        uart_tx_data_valid <= 1'b1;
                        //increment the word counter
                        word_counter <= word_counter + 1;
                        //increment the byte counter
                        byte_counter <= byte_counter + 1;
                    end
                endcase
                state <= WAIT_TX_DONE;
            end
            WAIT_TX_DONE:
            begin
                //reset the data valid flag
                uart_tx_data_valid <= 1'b0;
                if(uart_tx_done) begin
                    if(byte_counter == 4) begin
                        state <= READ_END_CHECK;
                    end
                    else begin
                        state <= SEND_WORD;
                    end
                end
            end
            READ_END_CHECK:
            begin
                //check if we need to send more words
                if(word_counter < word_limit) begin
                    state <= READ_WORD_0;
                end
                else begin
                    state <= GET_CMD;
                end
            end
            START_CPU:
            begin
                //release the CPU
                _comet_rst <= 1'b0;
                _comet_stall <= 1'b0;
                _comet_en <= 1'b1;
                //release control of the memory mux
                _imem_mux_select <= 1'b0;
                //clean the memory bus signals
                _inst_mem_addr <= 0;
                _inst_mem_data_in <= 0;
                _inst_mem_we <= 0;
                //release control of the UART mux
                _uart_mux_sel <= 1'b0;
            end
        endcase
    end

end

endmodule
