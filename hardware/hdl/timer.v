module timer (
    input clk,
    input rstn,
    input select,
    input [31:0] data_in,
    output reg [31:0] data_out,
    input we
);

reg [31:0] counter;
reg run;

always @(posedge clk) begin
    if(!rstn) begin
        counter <= 32'b0;
        run <= 1'b0;
        data_out <= 0;
    end
    else begin
        //update counter
        if(run) begin
            counter <= counter+1;
        end
        data_out <= 0;
        if(select) begin
            if(we) begin
                `ifdef SIMULATION
                    $display("[%0t] Timer write access", $realtime);
                `endif
                if(data_in == 32'h01) begin
                    run <= 1'b1;
                end
                else begin
                    run <= 1'b0;
                end
            end
            else begin
                `ifdef SIMULATION
                    $display("[%0t] Timer read access", $realtime);
                `endif
                data_out <= counter;
            end
        end
    end
end

endmodule
