module core_manager_access (
    input clk,
    input rstn,
    //memory IF
    input select,
    input [31:0] data_in,
    output [31:0] data_out,
    input we,
    //core manager reset signal
    output reg cm_rstn
);

assign data_out = 32'h00000000;

always @(posedge clk) begin
    if(!rstn) begin
        cm_rstn <= 1'b1;
    end
    else begin
        cm_rstn <= 1'b1;
        if(select) begin
            if(we) begin
                cm_rstn <= !data_in[0];
                //if we're simulating, stop the sim
                `ifdef SIMULATION
                    $display("[%0t] Core Manager access register written, stopping sim", $realtime);
                    $finish;
                `endif
            end
        end
    end
end

endmodule
