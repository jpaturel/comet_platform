module mm_uart #(
    parameter PRESCALER = 100
) (
    input clk,
    input rstn,
    //uart external IO
    input uart_rx,
    output uart_tx,
    //cpu bus
    input select,
    input [31:0] data_in,
    output reg [31:0] data_out,
    input we
);

reg tx_data_valid;
wire [7:0] rx_data;
wire rx_valid, tx_done;

always @(posedge clk) begin
    if(!rstn) begin
        tx_data_valid <= 1'b0;
        data_out <= 0;
    end
    else begin
        tx_data_valid <= 1'b0;
        data_out <= 0;
        if(select) begin
            if(we) begin
                //new char write
                //only output if tx not transmitting
                if(tx_done) begin
                    tx_data_valid <= 1'b1;
                    `ifdef SIMULATION
                        $display("%s", data_in[7:0]);
                    `endif
                end
            end
            else begin
                //read, expose tx and rx status
                //last 8b: received char
                //b8: RX valid
                //b9: TX ready
                data_out <= {22'b0, tx_done, rx_valid, rx_data};
            end
        end
    end
end


uart_rx #(.CLKS_PER_BIT(PRESCALER)) uart_rx_inst (  .i_Clock(clk),
                                                    .i_Rx_Serial(uart_rx),
                                                    .o_Rx_Byte(rx_data),
                                                    .o_Rx_DV(rx_valid));

uart_tx #(.CLKS_PER_BIT(PRESCALER)) uart_tx_inst (  .i_Clock(clk),
                                                    .i_Tx_DV(tx_data_valid),
                                                    .i_Tx_Byte(data_in[7:0]),   //always tied to the memory bus, the data valid signal handles sampling
                                                    .o_Tx_Done(tx_done),
                                                    .o_Tx_Serial(uart_tx));

endmodule
