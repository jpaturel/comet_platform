/*
*   Dual port RAM block
*   if both ports decide to write at the same address: nothing happens
*   if a port reads the data that the other ports writes, we forward the
*   data to be written to the read port
*/

module mem_wrapper_2p #(
    parameter MEM_ADDR_WIDTH=13,    //8k addresses
    parameter MEM_DATA_WIDTH=32     //default of 32kB
) (
    input clk,
    //port A
    input sel_a,
    input [MEM_ADDR_WIDTH-1:0] addr_a,
    input [MEM_DATA_WIDTH-1:0] data_in_a,
    output [MEM_DATA_WIDTH-1:0] data_out_a,
    input we_a,
    //port B
    input sel_b,
    input [MEM_ADDR_WIDTH-1:0] addr_b,
    input [MEM_DATA_WIDTH-1:0] data_in_b,
    output [MEM_DATA_WIDTH-1:0] data_out_b,
    input we_b
);

//memory array
reg [MEM_DATA_WIDTH-1:0] mem [(2**MEM_ADDR_WIDTH)-1:0];

//wires and regs
reg [MEM_DATA_WIDTH-1:0] data_out_reg_a;
assign data_out_a = data_out_reg_a;
reg [MEM_DATA_WIDTH-1:0] data_out_reg_b;
assign data_out_b = data_out_reg_b;

//module operating logic

//init mem values to 0
integer i;
initial begin
    for (i=0; i<2**MEM_ADDR_WIDTH; i=i+1) mem[i] <= 0;
end

always @(posedge clk) begin
    if(sel_a) begin
        if(we_a) begin
            mem[addr_a] <= data_in_a;
        end
        else begin  //read
            data_out_reg_a <= mem[addr_a];
        end
    end
    if(sel_b) begin
        if(we_b) begin
            mem[addr_b] <= data_in_b;
            //$display("[%0t][DPRAM port B] write %x at address %x", $realtime, data_in_b, addr_b);
        end
        else begin  //read while write conflict
            data_out_reg_b <= mem[addr_b];
            //$display("[%0t][DPRAM port B] read %x at address %x", $realtime, mem[addr_b], addr_b);
        end
    end
end

endmodule
