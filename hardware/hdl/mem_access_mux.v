module mem_access_mux #(
    parameter MEM_ADDR_WIDTH = 13,
    parameter MEM_DATA_WIDTH = 32
) (
    //core mem bus
    input [MEM_ADDR_WIDTH-1:0] core_addr,
    input [MEM_DATA_WIDTH-1:0] core_data_in,
    output reg [MEM_DATA_WIDTH-1:0] core_data_out,
    input core_we,
    //core manager mem_bus
    input [MEM_ADDR_WIDTH-1:0] mgr_addr,
    input [MEM_DATA_WIDTH-1:0] mgr_data_in,
    output reg [MEM_DATA_WIDTH-1:0] mgr_data_out,
    input mgr_we,
    //mux output bus
    output reg [MEM_ADDR_WIDTH-1:0] mux_addr,
    output reg [MEM_DATA_WIDTH-1:0] mux_data_in,
    input [MEM_DATA_WIDTH-1:0] mux_data_out,
    output reg mux_we,
    //mux control port
    input select    //0: core, 1:core manager
);

always @(*) begin
    case(select)
        0:  //core selected
        begin
            mux_addr = core_addr;
            mux_data_in = core_data_in;
            core_data_out = mux_data_out;
            mux_we = core_we;
        end
        1:  //core manager selected
        begin
            mux_addr = mgr_addr;
            mux_data_in = mgr_data_in;
            mgr_data_out = mux_data_out;
            mux_we = mgr_we;
        end
    endcase
end
endmodule
