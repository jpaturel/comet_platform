module soc_top(
    //global signals
    input clk,
    input rstn,
    //core manager IO
    input cm_rx,
    output cm_tx,
    //uart IO
    input uart_rx,
    output uart_tx,
    //general purpose IO
    input [3:0] inputs,
    output [3:0] outputs
);

//instantiate the core manager
logic comet_rst, comet_stall;
logic [23:0] mgr_inst_mem_addr;
logic [31:0] mgr_inst_mem_data_in;
logic [31:0] mgr_inst_mem_data_out;
logic mgr_inst_mem_we;
logic inst_mem_mux_select;
logic cm_access_rstn;

wire cm_rstn;
assign cm_rstn = rstn & cm_access_rstn;

//core_manager #(.UART_PRESCALER(868)) core_manager_inst (
core_manager #(.UART_PRESCALER(434)) core_manager_inst (
    .clk(clk),
    .rstn(cm_rstn),
    .comet_rst(comet_rst),
    .comet_stall(comet_stall),
    .inst_mem_addr(mgr_inst_mem_addr),
    .inst_mem_data_in(mgr_inst_mem_data_in),
    .inst_mem_data_out(mgr_inst_mem_data_out),
    .inst_mem_we(mgr_inst_mem_we),
    .uart_rx(cm_rx),
    .uart_tx(cm_tx),
    .imem_mux_select(inst_mem_mux_select)); //don't need the UART MUX for now

//instantiate an instruction memory mux
logic [23:0] core_inst_mem_addr;
logic [31:0] core_inst_mem_data_in;
logic [31:0] core_inst_mem_data_out;
logic core_inst_mem_we;
logic [23:0] mux_inst_mem_addr;
logic [31:0] mux_inst_mem_data_in;
logic [31:0] mux_inst_mem_data_out;
logic mux_inst_mem_we;

mem_access_mux #(.MEM_ADDR_WIDTH(24)) inst_mem_mux_inst (
    .core_addr(core_inst_mem_addr),
    .core_data_in(core_inst_mem_data_in),
    .core_data_out(core_inst_mem_data_out),
    .core_we(core_inst_mem_we),
    .mgr_addr(mgr_inst_mem_addr),
    .mgr_data_in(mgr_inst_mem_data_in),
    .mgr_data_out(mgr_inst_mem_data_out),
    .mgr_we(mgr_inst_mem_we),
    .mux_addr(mux_inst_mem_addr),
    .mux_data_in(mux_inst_mem_data_in),
    .mux_data_out(mux_inst_mem_data_out),
    .mux_we(mux_inst_mem_we),
    .select(inst_mem_mux_select));


//instruction and data memory
//16k addresses, 32b elements ->64kB
logic [23:0] data_mem_addr;
logic [31:0] data_mem_addr_32;
logic [31:0] inst_mem_addr_32;
logic [31:0] data_mem_data_in;
logic [31:0] data_mem_data_out;
logic data_mem_we;
logic ram_select;

assign data_mem_addr_32 = {6'b000000, data_mem_addr,2'b00};
assign inst_mem_addr_32 = {6'b000000, mux_inst_mem_addr,2'b00};

mem_wrapper_2p #(.MEM_ADDR_WIDTH(15), .MEM_DATA_WIDTH(32)) ram_inst (
    .clk(clk),
    //port A, instruction bus
    .sel_a(1'b1),
    .addr_a(mux_inst_mem_addr[14:0]),
    .data_in_a(mux_inst_mem_data_in),
    .data_out_a(mux_inst_mem_data_out),
    .we_a(mux_inst_mem_we),
    //port B, data bus
    .sel_b(ram_select),
    .addr_b(data_mem_addr[14:0]),
    .data_in_b(data_mem_data_in),
    .data_out_b(data_mem_data_out),
    .we_b(data_mem_we));


//address decoder
logic output_port_select;
logic input_port_select;
logic cm_access_select;
logic timer_select;
logic uart_select;
always_comb begin
    ram_select = 1'b0;
    output_port_select = 1'b0;
    input_port_select = 1'b0;
    cm_access_select = 1'b0;
    timer_select = 1'b0;
    uart_select = 1'b0;
    casez(data_mem_addr_32)
        32'b00000000_0000000?_????????_????????: ram_select = 1'b1;
        32'b00000000_00000010_00000000_000000??: input_port_select = 1'b1; //4 bytes
        32'b00000000_00000010_00000000_000001??: output_port_select = 1'b1; //4 bytes
        32'b00000000_00000010_00000000_000010??: cm_access_select = 1'b1; //4 bytes
        32'b00000000_00000010_00000000_000011??: timer_select = 1'b1; //4 bytes
        32'b00000000_00000010_00000000_000100??: uart_select = 1'b1; //4 bytes
    endcase
end

//instantiate the core manager access port
logic [31:0] cm_access_data_out;
core_manager_access cm_access_inst (
    .clk(clk),
    .rstn(rstn),
    .select(cm_access_select),
    .data_in(data_mem_data_in),
    .data_out(cm_access_data_out),
    .we(data_mem_we),
    .cm_rstn(cm_access_rstn)
);

//instantiate a general purpose output port
logic [31:0] output_port_data_out; 

simple_output_port output_port_inst (
    .clk(clk),
    .rstn(rstn),
    .select(output_port_select),
    .data_in(data_mem_data_in),
    .data_out(output_port_data_out),
    .we(data_mem_we),
    .outputs(outputs));

//instantiate a general purpose input port
logic [31:0] input_port_data_out; 

simple_input_port input_port_inst (
    .clk(clk),
    .rstn(rstn),
    .select(input_port_select),
    .data_in(data_mem_data_in),
    .data_out(input_port_data_out),
    .we(data_mem_we),
    .inputs(inputs));

//instantiate a timer
logic [31:0] timer_data_out; 

timer timer_inst (
    .clk(clk),
    .rstn(rstn),
    .select(timer_select),
    .data_in(data_mem_data_in),
    .data_out(timer_data_out),
    .we(data_mem_we));

//instantiate a memory mapped UART
logic [31:0] uart_data_out; 

mm_uart #(.PRESCALER(434)) mm_uart_inst (
    .clk(clk),
    .rstn(rstn),
    .select(uart_select),
    .data_in(data_mem_data_in),
    .data_out(uart_data_out),
    .we(data_mem_we),
    //uart io
    .uart_tx(uart_tx),
    .uart_rx(uart_rx));

//instantiate the processor core
//crash flag omitted
logic [31:0] core_data_mem_data_out;
//assign core_data_mem_data_out = data_mem_data_out | output_port_data_out | timer_data_out | uart_data_out | input_port_data_out | cm_access_data_out;

//data bus forwarding
always_comb begin
    core_data_mem_data_out = 32'h00000000;
    casez(data_mem_addr_32)
        32'b00000000_0000000?_????????_????????: core_data_mem_data_out = data_mem_data_out;
        32'b00000000_00000010_00000000_000000??: core_data_mem_data_out = input_port_data_out;
        32'b00000000_00000010_00000000_000001??: core_data_mem_data_out = output_port_data_out;
        32'b00000000_00000010_00000000_000010??: core_data_mem_data_out = cm_access_data_out;
        32'b00000000_00000010_00000000_000011??: core_data_mem_data_out = timer_data_out;
        32'b00000000_00000010_00000000_000100??: core_data_mem_data_out = uart_data_out;
    endcase
end

//assign core_data_mem_data_out = data_mem_data_out | output_port_data_out | cm_access_data_out;
//assign core_data_mem_data_out = data_mem_data_out | output_port_data_out;

doCore comet_inst (
    .clk(clk),
    .rst(comet_rst),
    .globalStall(comet_stall),
    .en(1'b1),
    //instruction memory, clken signal not needed
    .imData_rsc_adr(core_inst_mem_addr),
    .imData_rsc_d(core_inst_mem_data_in),
    .imData_rsc_q(core_inst_mem_data_out),
    .imData_rsc_we(core_inst_mem_we),
    //data memory, clken signal not needed
    .dmData_rsc_adr(data_mem_addr[23:0]),
    .dmData_rsc_d(data_mem_data_in),
    .dmData_rsc_q(core_data_mem_data_out),
    .dmData_rsc_we(data_mem_we)
);

endmodule
