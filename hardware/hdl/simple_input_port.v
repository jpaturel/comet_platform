module simple_input_port (
    input clk,
    input rstn,
    input select,
    input [31:0] data_in,
    output reg [31:0] data_out,
    input we,
    input [3:0] inputs
);

always @(posedge clk) begin
    if(!rstn) begin
        data_out <= 32'b0;
    end
    else begin
        data_out <= 32'b0;
        if(select) begin
            if(we) begin
                `ifdef SIMULATION
                    $display("[%0t] Input Port write access", $realtime);
                `endif
                //nothing
            end
            else begin
                `ifdef SIMULATION
                    $display("[%0t] Input Port read access", $realtime);
                `endif
                data_out <= {28'b0, inputs};
            end
        end
    end
end

endmodule
