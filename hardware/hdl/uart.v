module uart #(
    parameter PRESCALER = 100
) (
    input clk,
    //rx IO
    input uart_rx,
    output [7:0] uart_rx_data,
    output uart_rx_data_valid,
    //tx IO
    output uart_tx,
    input [7:0] uart_tx_data,
    input uart_tx_data_valid,
    output uart_tx_done
);


uart_rx #(.CLKS_PER_BIT(PRESCALER)) uart_rx_inst (  .i_Clock(clk),
                                                    .i_Rx_Serial(uart_rx),
                                                    .o_Rx_Byte(uart_rx_data),
                                                    .o_Rx_DV(uart_rx_data_valid));

uart_tx #(.CLKS_PER_BIT(PRESCALER)) uart_tx_inst (  .i_Clock(clk),
                                                    .i_Tx_DV(uart_tx_data_valid),
                                                    .i_Tx_Byte(uart_tx_data),
                                                    .o_Tx_Done(uart_tx_done),
                                                    .o_Tx_Serial(uart_tx));

endmodule
