module simple_output_port (
    input clk,
    input rstn,
    input select,
    input [31:0] data_in,
    output reg [31:0] data_out,
    input we,
    output [3:0] outputs
);

reg [3:0] _outputs;
assign outputs = _outputs;

always @(posedge clk) begin
    if(!rstn) begin
        _outputs <= 4'b1111;
        data_out <= 32'b0;
    end
    else begin
        data_out <= 32'b0;
        if(select) begin
            if(we) begin
                `ifdef SIMULATION
                    $display("[%0t] Output Port write access", $realtime);
                `endif
                _outputs <= data_in[3:0];
            end
            else begin
                `ifdef SIMULATION
                    $display("[%0t] Output Port read access", $realtime);
                `endif
                data_out <= {28'b0, _outputs};
            end
        end
    end
end

endmodule
