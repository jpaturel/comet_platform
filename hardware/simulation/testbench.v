`timescale 1ns / 100ps

module testbench();

//localparam clk_period = 10; //100MHz
localparam clk_period = 20; //50MHz
localparam num_sim_cycles = 40000000;

//generate the input clock
reg clk = 1'b0;
always begin
    #(clk_period/2);
    clk = !clk;
end

//blip the reset line
reg rstn = 1'b1;
initial begin
   #(clk_period*10);
   rstn = 1'b0;
   #(clk_period*10);
   rstn = 1'b1;
end

//UART writing task, 115200 bauds (with resolution of 1ns)
//a symbol lasts 8680.5 ns
localparam symbol_time = 8681;
reg uart_data = 1'b1;
integer uart_state = 0;
task uart_send (input [7:0] data);
    integer i;
    begin
        //start bit
        uart_data <= 1'b0;
        uart_state = 1;
        #(symbol_time);
        //data bit 0 to 7
        uart_state = 2;
        for(i=0; i<8; i=i+1) begin
            uart_data <= data[i];
            #(symbol_time);
        end
        //stop bit
        uart_state = 3;
        uart_data <= 1'b1;
        #(symbol_time);
        uart_state = 0;
    end
endtask

reg [7:0] bin_data;
task upload_binary();
    integer dummy;
    integer bin_file;
    begin
        `ifndef IVERILOG
        //bin_file = $fopen("../../../../../../firmware/build/firmware_sim.dat", "rb");
        bin_file = $fopen("../../firmware/build/firmware_sim.dat", "rb");
        `else
        bin_file = $fopen("../firmware/build/firmware_sim.dat", "rb");
        `endif
        if(bin_file == 0) begin
            $display("Error opening .dat file");
            $display("File descriptor: %d", bin_file);
            $finish;
        end
        $display("[%0t] Uploading firmware", $realtime);
        while(!$feof(bin_file)) begin
            dummy = $fscanf(bin_file, "%h\n", bin_data);
            uart_send(bin_data);
        end
        $display("[%0t] Done uploading firmware", $realtime);
    end
endtask

//instantiate the SoC
wire uart_tx;
wire [3:0] outputs;
soc_top DUT (
    .clk(clk),
    .rstn(rstn),
    .cm_rx(uart_data),
    .cm_tx(uart_tx),
    //peripherals
    .inputs(4'b0000),
    .outputs(outputs));

initial begin
    `ifdef IVERILOG
    $dumpfile(`DUMPFILE);
    $dumpvars(0, testbench);
    `endif
    $timeformat (-9, 5, "ns", 5);
    //wait a bit for the reset to be effective
    #(clk_period*20);
    //upload the binary
    upload_binary();
    //start the CPU
    $display("[%0t] Starting CPU", $realtime);
    uart_send(8'hfa);
    //wait for the SoC to do it's thing
    #(num_sim_cycles*clk_period);
    $finish;
end

endmodule
