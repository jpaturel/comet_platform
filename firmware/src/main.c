#include "comet.h"

//UART reg:
//22'b0, tx_done, rx_valid, data_8

void uart_putchar(char c) {
    //don't write if the TX is busy
    while(!(UART->STAT_DAT & (1 << 1)))
        ;
    UART->STAT_DAT = c;
    //wait untill the character is fully written
    while(!(UART->STAT_DAT & (1 << 1)))
        ;
}

void delay(int reps) {
    for(int i=0; i<reps; i++)
        __asm("nop");
}

int main(void) {
    //turn the LEDs on
    OP->DR = 2;

    //write something on the UART
    //uart_putchar('T'); 

    //and release control
    CM_TAKEOVER;
   return 0;
}
