#include "custom_ops.h"

uint8_t GF_256_mul(uint8_t a, uint8_t b) {
    uint8_t res;
    //GF 256 multiplication
    //R-type instruction, funct7=0, funct3=0, opcode=0x0b
    asm(".insn r 0x0b, 0, 0, %[result], %[val_a], %[val_b]"
        :[result] "=r" (res)
        :[val_a] "r" (a), [val_b] "r" (b));
    return res;
}
