# 0 "crt0.S"
# 0 "<built-in>"
# 0 "<command-line>"
# 1 "crt0.S"
# 13 "crt0.S"
.section .text.start
.global _start
.type _start, @function

_start:

.option push
.option norelax
1: auipc gp, %pcrel_hi(__global_pointer$)
 addi gp, gp, %pcrel_lo(1b)
.option pop


 la sp, _sp


 la a0, __vector_start
        ori a0, a0, 0x1
 csrw mtvec, a0


 la a0, __bss_start
 la a2, __bss_end
 sub a2, a2, a0
 li a1, 0
 call memset




 call __libc_init_array


 lw a0, 0(sp)
 addi a1, sp, 8
 li a2, 0
 call main
 tail exit

.size _start, .-_start

.global _init
.type _init, @function
.global _fini
.type _fini, @function
_init:
_fini:


 ret
.size _init, .-_init
.size _fini, .-_fini
