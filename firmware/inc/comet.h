#ifndef __COMET_H__
#define __COMET_H__

#include <stdint.h>

//output port
typedef struct {
    uint32_t DR;     //data register, only the lower 4 bits are tied to actual outputs
} OP_t;

#define OUTPUT_PORT_BASE_ADDRESS ((uint32_t) 0x00020004)
#define OP ((OP_t *) OUTPUT_PORT_BASE_ADDRESS)

//input port
typedef struct {
    uint32_t DR;     //data register, only the lower 4 bits are tied to actual inputs
} IP_t;

#define INPUT_PORT_BASE_ADDRESS ((uint32_t) 0x00020000)
#define IP ((IP_t *) INPUT_PORT_BASE_ADDRESS)

//core manager access port
typedef struct {
    uint32_t DR;     //there's only one usable bit in this register
} CM_ACCESS_t;

#define CM_ACCESS_BASE_ADDRESS ((uint32_t) 0x00020008)
#define CM_ACCESS ((CM_ACCESS_t *) CM_ACCESS_BASE_ADDRESS)
#define CM_TAKEOVER CM_ACCESS->DR = 0xffffffff;

//timer
typedef struct {
    uint32_t CTRL_CNT;
} TIMER_t;

#define TIMER_BASE_ADDRESS ((uint32_t) 0x0002000C)
#define TIMER ((TIMER_t *) TIMER_BASE_ADDRESS)

//UART
typedef struct {
    uint32_t STAT_DAT;
} UART_t;

#define UART_BASE_ADDRESS ((uint32_t) 0x00020010)
#define UART ((UART_t *) UART_BASE_ADDRESS)

#endif /* ifndef __COMET_H__ */
