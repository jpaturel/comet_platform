#ifndef __CUSTOM_OPS_H__
#define __CUSTOM_OPS_H__ 

#include <stdint.h>

uint8_t GF_256_mul(uint8_t a, uint8_t b);   //with AES irreducible polynomial

#endif /* ifndef __CUSTOM_OPS_H__ */
