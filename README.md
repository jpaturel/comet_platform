# comet_platform

A simple test platform for [Comet](https://gitlab.inria.fr/srokicki/Comet)

## Repo contents

This repo contains the files needed to implement a simple Comet instance with some memory, an output port and a "core manager" (to access memory while the processor is stopped, to start the processor, etc).

> There is no core distributed via this repo, you'll have to generate your own and place it in `hardware/hdl/comet.v` for the platform to work

This platfrom has been tested on a [Digilent ArtyA7-35T](https://digilent.com/shop/arty-a7-artix-7-fpga-development-board/) board.

## Platform Description

This platform integrates a RISCV CPU (Comet), a dual port memory that holds both the program and the data it operates on, an output port and a "core manager".

The core manager handles the core reset and enable signals and is also tied to the memory through a multiplexer (sharing the acces port of the CPU instruction bus). the manager communicates using a UART and supports these commands:

| Command name | Opcode | Syntax                                                                   | Behavior                                                                                                       |
|--------------|--------|--------------------------------------------------------------------------|----------------------------------------------------------------------------------------------------------------|
| WRITE        | OxAA   | <opcode> <start address> <transfer length (in 32b words)> <... data ...> | Writes <transfer length> 32 bit words starting at <start address>                                              |
| READ         | 0x55   | <opcode> <start address> <transfer length (in 32b words)>                | Reads <transfer length> 32 bit words starting at <start address>, the data is outputted byte-wise LSB first    |
| START        | 0xFA   | <opcode>                                                                 | Releases the CPU reset and enable lines and gives control to the memory access port to the CPU instruction bus |

## Building the hardware

> TODO

## Building the software

In order to build binaries for the comet core you'll need a risc-v toolchain, you can follow the instructions listed in the [Comet](https://gitlab.inria.fr/srokicki/Comet) repo.

An example program is provided in this repo, you can find it in the `firmware` folder.

To build it, `cd` into `firmware` and run `make`.
> This will also create a file that can be used for behavioral simulation of the platform

> The tool used to create the simulation binary can be found in `tools/sim_blob_creator`

## Flashing a firmware

While in the `firmware` folder, run `make upload`. This will call `tools/elf_loader` and communicate with the core manager, uploading the program, checking that every has been written properly and then resetting the CPU

## Inspecting memory

The `mem_inspector` script can be found in the `tools` directory. It communicates with the core manager to read the memory of the platform.

Usage: `mem_inspector <address> <size_in_bytes> <serial_port>`

> Note that addresses and sizes can be specified in hex or decimal
